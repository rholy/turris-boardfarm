# Copyright (c) 2016
#
# All rights reserved.
#
# This file is distributed under the Clear BSD license.
# The full text can be found in LICENSE in the root directory.

import re
import rootfs_boot
import time
from devices import board, wan, lan, wlan, prompt

class TurrisFactoryReset(rootfs_boot.RootFSBootTest):
    '''Reset Turris to factory state.'''
    def runTest(self):
        board.reset_initial()
        board.set_branch()
        board.sendline('updater.sh -n')
        i = 1
        while(i < 4):
            i = board.expect(['opkg', 'curl', 'sleep', '+ grep'] + prompt, timeout=180)
        board.sendline('\necho First updater finished running')
        board.set_branch()
        maybe_reboot = 1
        while(maybe_reboot > 0):
            board.sendline('cat /tmp/update-state/state')
            board.expect('done', timeout=2)
            board.expect(prompt)
            board.sendline('if test -f /tmp/offline-update-ready; then uname -s; else nslookup localhost; fi')
            maybe_reboot = board.expect(['127.0.0.1', 'Linux'])
            board.expect(prompt)
            if(maybe_reboot > 0):
                board.sendline('reboot')
                board.expect('Router Turris successfully started', timeout=180)
                time.sleep(5)
                i = 1
                while(i > 0):
                    board.sendline('\ncat /tmp/update-state/state')
                    i = board.expect(['done', 'examine', 'install', 'cooldown', 'sleep', 'startup', 'get list', 'remove'])
                    time.sleep(5)
                board.set_branch()
        board.sendline('uci set foris.wizard=config')
        board.expect(prompt)
        board.sendline('uci set foris.wizard.allowed_step_max=9')
        board.expect(prompt)
        board.sendline('uci set foris.wizard.finished=1')
        board.expect(prompt)
        board.sendline('uci set foris.auth=config')
        board.expect(prompt)
        board.sendline("uci set foris.auth.password='$p5k2$3e8$1f8L.JQR$3P1sKJqBZ0PTb5UKKrmHGoL8IVKxvjch'") # Turris
        board.expect(prompt)
        board.sendline('uci commit')
        board.expect(prompt)

    def recover(self):
        board.sendline('cat /tmp/update-state/last_error /tmp/update-state/last_error')
        board.close()
        lib.common.test_msg("Unable to finish factory reset, skipping remaining tests...")


class TurrisUpdate(rootfs_boot.RootFSBootTest):
    '''Updates Turris within correct branch.'''
    def runTest(self):
        board.reset(break_into_uboot=False)
        board.expect('Please press Enter to activate this console.', timeout=250)
        board.expect('Router Turris successfully started', timeout=250)
        time.sleep(15)
        board.sendline('')
        board.set_branch()
        board.sendline('updater.sh -n > /dev/null 2> /dev/null &')
        while(i > 1):
            board.sendline('\ncat /tmp/update-state/state')
            i = board.expect(['done', 'error', 'examine', 'install', 'cooldown', 'sleep', 'startup', 'get list', 'remove'], timeout=3)
            time.sleep(5)
        if(i > 0):
            raise Exception('Update failed')
        board.expect(prompt)
        board.set_branch()
