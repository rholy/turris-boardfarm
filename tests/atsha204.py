# Copyright (c) 2015
#
# All rights reserved.
#
# This file is distributed under the Clear BSD license.
# The full text can be found in LICENSE in the root directory.

import time

import rootfs_boot
from devices import board, wan, lan, wlan, prompt

class ATSHA204Info(rootfs_boot.RootFSBootTest):
    '''Check that ATSHA204 returns serial number, HW revision and MAC.'''
    def runTest(self):
        board.sendline('\natsha204cmd serial-number | sed -e "s|^|_-=|" -e "s|$|=-_|"')
        board.expect('_-=[0-9A-F]{16}=-_')
        board.expect(prompt)
        board.sendline('\natsha204cmd hw-rev | sed -e "s|^|_-=|" -e "s|$|=-_|"')
        board.expect('_-=[0-9A-F]{8}=-_')
        board.expect(prompt)
        board.sendline('\natsha204cmd mac 1 | sed -e "s|^|_-=|" -e "s|$|=-_|"')
        board.expect('_-=([0-9A-F]{2}:){5}[0-9A-F]{2}=-_')
        board.expect(prompt)

