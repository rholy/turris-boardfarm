# Copyright (c) 2015
#
# All rights reserved.
#
# This file is distributed under the Clear BSD license.
# The full text can be found in LICENSE in the root directory.

import common
import os
import time
import openwrt_router

class TurrisRouter(openwrt_router.OpenWrtRouter):

    def __init__(self, *args, **kwargs):
        super(TurrisRouter, self).__init__(*args, **kwargs)
        self.wan_iface = "eth2"
        self.lan_iface = "eth0 eth1"
        self.prompt = ['root\\@.*:.*# ']
        self.uprompt = ['=> ']

    def reset_initial(self):
        self.reset(break_into_uboot=True)
        self.sendline('\n')
        self.sendline('run norboot')
        self.expect('Welcome in rescue mode')
        self.expect('Erased')
        self.expect('Flash new images')
        self.expect('Root filesystem flashed.', timeout=300)
        self.expect('U-Boot')
        self.expect('Please press Enter to activate this console.', timeout=150)
        self.expect('procd: - init complete -')
        self.sendline()
        self.expect_prompt()

    def set_branch(self):
        '''Method to change the branch - kinda ugly, depends on environment

        Requires:
        - the board
          - containing Turris OS 3.8 or higher
        - a BRANCH environment variable set to the name of the desired branch

        '''
        branch = os.getenv('BRANCH', 'deploy')
        self.sendline('switch-branch {}'.format(branch))
        self.expect_prompt(timeout=90)

class OmniaRouter(TurrisRouter):
    def __init__(self, *args, **kwargs):
        super(OmniaRouter, self).__init__(*args, **kwargs)
        self.wan_iface = "eth1"
        self.lan_iface = "eth0 eth2"

    def reset_initial(self):
        self.reset(break_into_uboot=True)
        self.sendline('setenv rescue 2')
        self.expect(self.uprompt)
        self.sendline('run rescueboot')
        self.expect_exact('Rolled back to snapshot factory', timeout=25)
        self.expect_exact('Router Turris successfully started.', timeout=100)
        self.sendline()
        self.expect_prompt()
